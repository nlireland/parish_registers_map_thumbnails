
# ParishRegistersMapThumbnails

TODO: Write a gem description


## Installation

Add this line to your application's Gemfile:


```ruby
gem 'parish_registers_map_thumbnails'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install parish_registers_map_thumbnails


## Usage

TODO: Write usage instructions here


## Contributing


1. Fork it ( https://github.com/[my-github-username]/parish_registers_map_thumbnails/fork )

2. Create your feature branch (`git checkout -b my-new-feature`)

3. Commit your changes (`git commit -am 'Add some feature'`)

4. Push to the branch (`git push origin my-new-feature`)

5. Create a new Pull Request


## Running Script

This script generates the map images using a mapbox api.

To script can take 2 arguments:

1. overwrite: set to "true" or "false"
2. parish_id: "0011" OR "0011, 0022"

To run the script we have the following options below:

1. Checks if all the parishes exist - if not write them
2. Checks if all the parishes exist - if not write them
3. Checks if passed in parishes exist - if not write them 
4. Overwrites all parishes
5. Overwrites passed in parishes

```
ruby lib/generate_map_thumbnails.rb
ruby lib/generate_map_thumbnails.rb "false"
ruby lib/generate_map_thumbnails.rb "false" "0011, 0022"
ruby lib/generate_map_thumbnails.rb "true"
ruby lib/generate_map_thumbnails.rb "true" "0011, 0022"
```
