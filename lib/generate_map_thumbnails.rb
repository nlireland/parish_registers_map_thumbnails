require 'fileutils'
require 'yaml'
require 'open-uri'
require 'net/http'
require 'json'
require 'timeout'


CONFIG_FILE = YAML.load_file(File.dirname(__FILE__)+'/../config/config.yml')

FileUtils::mkdir CONFIG_FILE['map_thumbnails_image_path'] unless Dir.exists? CONFIG_FILE['map_thumbnails_image_path']


# METHODS

def call_mapbox_to_create_image(parish_id, lat, lng)

  mapbox_api_url = "#{CONFIG_FILE['mapbox_base_path']}/#{CONFIG_FILE['mapbox_file_path']}/pin-s+f44(#{lng},#{lat})/-8.0475,53.4072,6/280x360.jpg?access_token=#{CONFIG_FILE['mapbox_access_token']}"

  thumbnail_file_path = "#{CONFIG_FILE['map_thumbnails_image_path']}" + "#{parish_id}" + ".jpg"

  retries = 3
  begin
    Timeout::timeout(10) do
      open(mapbox_api_url){|mapbox_content|
        if mapbox_content.status[0] == '200'
          open(thumbnail_file_path, 'wb') do |file|
            file << mapbox_content.read
          end
        end
      }
    end
  rescue Timeout::Error => e
    if retries > 0
      puts "Timeout - Retrying..."
      retries -= 1
      retry
    else
      puts "#{e.message} - We cannot connect to Mapbox to generate the image for parish ID: #{parish_id}"
      exit
    end
  rescue OpenURI::HTTPError => e
    puts "#{e.message} - Error from mapbox for: #{parish_id}"
  end
end

def image_exists?(parish_id)
  File.exist?(CONFIG_FILE['map_thumbnails_image_path'] + "#{parish_id}" + '.jpg') ? true : false
end

def query_solr_for_all_parish_info
  url = URI(CONFIG_FILE['solr_query_path'])
  solr_response = Net::HTTP.get(url)

  solr_parsed = JSON.parse(solr_response)

  return solr_parsed
end

def loop_through_solr(overwrite, parish_ids_to_write=nil)
  solr_parish_info = query_solr_for_all_parish_info

  solr_parish_info["response"]["docs"].each do |item|
    parish_id = item["id"]
    if(parish_id != nil && item["lat_lng_center_ss"] != nil)
      lat, lng = item["lat_lng_center_ss"].split(",")
      if parish_ids_to_write == nil || parish_ids_to_write.include?(parish_id)
        if(overwrite == "false" || overwrite == nil)
          unless image_exists?(parish_id)
            call_mapbox_to_create_image(parish_id, lat, lng)
          end
        else
          call_mapbox_to_create_image(parish_id, lat, lng)
        end
      end
    end
  end
end

def integer?(str)
  /\A[+-]?\d+\z/ === str
end


# CODE EXECUTION

overwrite = ARGV[0].downcase
parish_ids_to_write = ARGV[1]


# Check the first argument is either true or false

if overwrite != nil
  unless overwrite == "true" || overwrite == "false"
    puts "The first argument must be either true or false - NO IMAGES WERE SAVED"
    exit
  end
end


# Check the second argument is a string of valid parish ID's

if parish_ids_to_write != nil
  parish_ids_to_write = parish_ids_to_write.split(",")
  parish_ids_to_write.each{|id| id.strip!}

  input_error = false
  parish_ids_to_write.each do |parish_id|
    unless ((integer?(parish_id)) && (parish_id.length == 4))
      puts parish_id.to_s + " - is not a valid 4 digit parish ID - please enter valid ID's as a string in 4 digit form e.g. "'"0011, 0022, 0032"'" - NO IMAGES WERE SAVED"
      input_error = true
    end
  end
  exit if input_error
end


# LETS GO
loop_through_solr(overwrite, parish_ids_to_write)
