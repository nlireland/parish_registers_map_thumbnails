# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'parish_registers_map_thumbnails/version'

Gem::Specification.new do |spec|
  spec.name          = "parish_registers_map_thumbnails"
  spec.version       = ParishRegistersMapThumbnails::VERSION
  spec.authors       = ["NLI", "Rory Egerton"]
  spec.email         = ["ithelpdesk@nli.ie"]
  spec.summary       = "This is a gem to provide the map thumbnails to parish_registers application."
  spec.description   = "This is a gem to generate and store the map thumbnails for each parish."
  spec.homepage      = "https://bitbucket.org/nlireland/parish_registers_map_thumbnails"
  spec.license       = "AGPL version 3"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake"
end
